package es.arjona.u3t3menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ItemDetailActivity extends AppCompatActivity {

    private TextView textView1;

    private String activityName;
    public ItemDetailActivity(String activityName) {
        this.activityName =  activityName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        setUI();

        try {
            launchIntent(activityName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void launchIntent(String activityName) throws ClassNotFoundException {
        new Intent(this, Class.forName(getPackageName()+ "." + activityName));
    }

    public void setUI() {
        textView1 = findViewById(R.id.textView);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        textView1.setText("Hello I'm the activity  ");
    }
}